import crypto from 'crypto-js';

const createAdmin = () => {
    localStorage.setItem('users', JSON.stringify({ login: 'admin', password: crypto.SHA256('admin') }))
};

export default createAdmin;