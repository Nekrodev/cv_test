function login(login, password) {
  const users = JSON.parse(localStorage.getItem('users'));
  const user = users.find( x => x.login === login);

  if (!user) {
    throw Error("Invalid login or password");
  }

  if (user.password === password) {
    return;
  }

  throw Error("Invalid login or password");
}

export default login;
