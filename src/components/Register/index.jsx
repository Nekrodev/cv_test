import "antd/dist/antd.css";
import { Form, Input, Button } from "antd";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const Login = () => {
  const handleFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div style={{ maxWidth: "50%" }}>
      <Form
        {...layout}
        name="basic"
        onFinish={handleFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Registration"
          name="register"
          rules={[
            {
              required: true,
              message: "Please write your email!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please write your password!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Repeat password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please write your password!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Login
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;
