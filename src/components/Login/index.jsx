import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { login } from "./../../actions/login";
import Login from "./Login";

const ConnectedLogin = () => {
  const dispatch = useDispatch();

  const handleSubmit = useCallback(
    (props) => {
      dispatch(login(props.login, props.password));
    },
    [dispatch]
  );

  return <Login onFinish={handleSubmit} />;
};

export default ConnectedLogin;
