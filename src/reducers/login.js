import { combineReducers } from "redux";

import * as loginActions from "./../constants/actions/login";

const INITIAL_STATE = {
  error: null,
  isLoading: false,
  login: "",
};

const login = (state = INITIAL_STATE.login, action) => {
  switch (action.type) {
    case loginActions.LOGIN_SUCCESSFUL:
      return action.payload.login;
    case loginActions.LOGIN_FAILED:
    case loginActions.LOGOUT:
    case loginActions.LOGIN:
      return INITIAL_STATE.login;
    default:
      return state;
  }
};

const error = (state = INITIAL_STATE.error, action) => {
  switch (action.type) {
    case loginActions.LOGIN_FAILED:
      return action.error;
    case loginActions.LOGIN_SUCCESSFUL:
    case loginActions.LOGOUT:
    case loginActions.LOGIN:
      return INITIAL_STATE.error;
    default:
      return state;
  }
};

const isLoading = (state = INITIAL_STATE.isLoading, action) => {
  switch (action.type) {
    case loginActions.LOGIN:
      return true;
    case loginActions.LOGIN_FAILED:
    case loginActions.LOGOUT:
    case loginActions.LOGIN_SUCCESSFUL:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  login,
  error,
  isLoading,
});
