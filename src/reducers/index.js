import { combineReducers } from "redux";
import cv from "./cv";
import login from "./login";
import registration from "./registration";

export default combineReducers({
  cv,
  login,
  registration,
});
