import { combineReducers } from "redux";

import * as cvActions from "./../constants/actions/cv";

const INITIAL_STATE = {
  error: null,
  isLoading: false,
  content: {},
};

const content = (state = INITIAL_STATE.content, action) => {
  switch (action.type) {
    case cvActions.CV_SAVED:
    case cvActions.CV_LOADED:
      return action.payload.content;
    default:
      return state;
  }
};

const error = (state = INITIAL_STATE.error, action) => {
  switch (action.type) {
    case cvActions.CV_LOADING_FAILED:
    case cvActions.CV_SAVING_FAILED:
      return action.error;
    case cvActions.LOAD_CV:
    case cvActions.SAVE_CV:
    case cvActions.CV_LOADED:
    case cvActions.CV_SAVED:
      return INITIAL_STATE.error;
    default:
      return state;
  }
};

const isLoading = (state = INITIAL_STATE.isLoading, action) => {
  switch (action.type) {
    case cvActions.LOAD_CV:
    case cvActions.SAVE_CV:
      return true;
    case cvActions.CV_LOADED:
    case cvActions.CV_LOADING_FAILED:
    case cvActions.CV_SAVED:
    case cvActions.CV_SAVING_FAILED:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  content,
  error,
  isLoading,
});
