import { combineReducers } from "redux";

import * as registrationActions from "./../constants/actions/registration";

const INITIAL_STATE = {
  error: null,
  isLoading: false,
};

const error = (state = INITIAL_STATE.error, action) => {
  switch (action.type) {
    case registrationActions.REGISTRATION_FAILED:
      return action.error;
    case registrationActions.REGISTRATION_SUCCESSFUL:
    case registrationActions.REGISTER:
      return INITIAL_STATE.error;
    default:
      return state;
  }
};

const isLoading = (state = INITIAL_STATE.isLoading, action) => {
  switch (action.type) {
    case registrationActions.REGISTRATION_FAILED:
    case registrationActions.REGISTRATION_SUCCESSFUL:
      return false;
    case registrationActions.REGISTER:
      return true;
    default:
      return state;
  }
};

export default combineReducers({
  error,
  isLoading,
});
