/* eslint-disable react/jsx-no-bind, react/no-multi-comp*/

import React from "react";
import { Route, Switch } from "react-router-dom";
import Login from './components/Login/Login';
import Register from './components/Register';
import Resume from './components/Resume';
import ROUTES from "./constants/routes";

function Root() {
  return (
        <Switch>
          <Route path={ROUTES.cv}>
            <Resume />
          </Route>
          <Route path={ROUTES.register}>
            <Register />
          </Route>
          <Route path={ROUTES.login}>
            <Login />
          </Route>
        </Switch>
  );
}

export default Root;
