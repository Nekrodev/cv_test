export const SAVE_CV = "cv/SAVE_CV";
export const CV_SAVED = "cv/CV_SAVED";
export const CV_SAVING_FAILED = "cv/CV_SAVING_FAILED";

export const LOAD_CV = "cv/LOAD_CV";
export const CV_LOADED = "cv/CV_LOADED";
export const CV_LOADING_FAILED = "cv/CV_LOADING_FAILED";
