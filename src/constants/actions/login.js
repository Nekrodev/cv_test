export const LOGIN = "login/LOGIN";
export const LOGOUT = "login/LOGOUT";
export const LOGIN_SUCCESSFUL  = "login/LOGIN_SUCCESSFUL";
export const LOGIN_FAILED = "login/LOGIN_FAILED";