const ROUTES = {
    login : '/',
    cv    : '/cv',
    register : '/register'
}

export default ROUTES;
