import { takeLatest, put } from "redux-saga/effects";
import { loginSuccessful, loginFailed } from "../actions/login";
import { LOGIN } from "../constants/actions/login";
import loginService from "../services/login";

function* loginSaga(action) {
  try {
    const { login, password } = action.payload;
    loginService(login, password);
    yield put(loginSuccessful());
  } catch (e) {
    yield put(loginFailed(e));
  }
}

function* watchLogin() {
  yield takeLatest(LOGIN, loginSaga);
}

export default watchLogin;
