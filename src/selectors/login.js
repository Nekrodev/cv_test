export const getLogin = (state) => state.login;

export const getLoginLoadingState = (state) => ({
  isLoading: state.isLoading,
  error: state.error,
});
