import "./App.css";
import React, { useEffect } from "react";
import "antd/dist/antd.css";
import "./index.css";
import { Layout, Menu } from "antd";
import { BrowserRouter } from "react-router-dom";
import Root from "./Root";
import ROUTES from "./constants/routes";
import { useHistory } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import createAdmin from "./services/createAdmin";

const { Header, Content, Footer } = Layout;

function App() {
  const history = useHistory();

  useEffect(createAdmin, []);

  const handleClick = (props) => {
    history.push(props.key);
  };

  return (
    <Layout>
      <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={[ROUTES.login]}
          onClick={handleClick}
        >
          <Menu.Item key={ROUTES.login}>Login </Menu.Item>
          <Menu.Item key={ROUTES.register}>Register</Menu.Item>
          <Menu.Item key={ROUTES.cv}>My resume</Menu.Item>
        </Menu>
      </Header>
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 380 }}
        >
          <Root />
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>Created by Nekrodev</Footer>
    </Layout>
  );
}

const WrappedApp = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  );
};

export default WrappedApp;
