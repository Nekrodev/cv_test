import * as cvActions from "./../constants/actions/cv";

export const saveCV = (content) => ({
  type: cvActions.SAVE_CV,
  payload: { content },
});

export const CVSavingFailed = (error) => ({
  type: cvActions.CV_SAVING_FAILED,
  error
});

export const CVSaved = () => ({
  type: cvActions.CV_SAVED,
});

export const loadCV = (content) => ({
  type: cvActions.SAVE_CV,
  payload: { content },
});

export const CVLoadingFailed = (error) => ({
  type: cvActions.CV_LOADING_FAILED,
  error
});

export const CVLoaded = () => ({
  type: cvActions.CV_LOADED,
});
