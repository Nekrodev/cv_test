import * as registrationActions from "./../constants/actions/registration";

export const register = (login, password) => ({
  type: registrationActions.REGISTER,
  payload: { login, password },
});

export const registerFailed = (error) => ({
  type: registrationActions.REGISTRATION_FAILED,
  error
});

export const registerSuccessful = () => ({
  type: registrationActions.REGISTRATION_SUCCESSFUL,
});
