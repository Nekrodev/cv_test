import * as loginActions from "./../constants/actions/login";

export const login = (login, password) => ({
  type: loginActions.LOGIN,
  payload: { login, password },
});

export const loginFailed = (error) => ({
  type: loginActions.LOGIN_FAILED,
  error
});

export const loginSuccessful = () => ({
  type: loginActions.LOGIN_SUCCESSFUL,
});

export const logout = () => ({
  type: loginActions.LOGOUT,
});
